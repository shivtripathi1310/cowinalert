package cowin.test;

import java.util.Timer;
import java.util.TimerTask;

import cowin.base.GlobalVariable;

/**
 * This is the Main class that has to be run
 * This class will run the Cowin site at every duration of 1min (default).
 * 
 * @author Shiv_Tripathi
 *
 */
public class Scheduler extends TimerTask {

    private int noOfMin;
    CowinTest test =  new CowinTest();

    public int getNoOfMins() {
        return noOfMin;
    }

    public void setNoOfMin(int noOfMin) {
        this.noOfMin = noOfMin;
    }

    public static void main(String args[]) {
        Timer timer = new Timer();
        Scheduler scheduler = new Scheduler();
        // Set number of minutes
        scheduler.setNoOfMin(GlobalVariable.ITERATION_TIME_MIN);
        
        // No of minutes in milliseconds
        timer.schedule(scheduler, 0, scheduler.getNoOfMins() * 1000 * 60);
    }

    @Override
    public void run() {
        System.out.println("Task is running every " + this.getNoOfMins() + " minutes");
        try {
			test.cowinTest();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
}