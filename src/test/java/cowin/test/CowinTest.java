package cowin.test;

import java.util.Iterator;
import java.util.List;

import org.testng.annotations.Test;

import cowin.base.GlobalVariable;
import cowin.base.Page;
import cowin.page.HomePage;
import cowin.utility.CommonUtilities;

public class CowinTest extends BaseTest {

	@Test
	public void cowinTest() throws InterruptedException {
		BaseTest.message = "";
		Page.setup(GlobalVariable.BROWSER);

		HomePage homePage =  new HomePage(Page.driver);
		homePage.selectAreaByDistrict(stateName,districtName);

		List<String> allSlotsList = homePage.searchSlotsForAgeAndDate(age,districtName,dateOfVaccine);
		
		statusForSlots = generateMessageToSendForEmail(statusForSlots, allSlotsList);

		if (statusForSlots)
			CommonUtilities.sendMail(Page.recepientMails,BaseTest.message);
		
		tearDown();
	}

	private boolean generateMessageToSendForEmail(boolean statusForSlots, List<String> allSlotsList) {
		Iterator<String> itr = allSlotsList.iterator();
		String data;
		while (itr.hasNext()) {
			data = itr.next();
			if (!(data.contains("Booked") || data.contains("NA"))) {
				BaseTest.message = BaseTest.message + "\n<br/>" + data;
				statusForSlots = true;
			}
		}
		System.out.println("Message is :\n " + BaseTest.message);
		return statusForSlots;
	}

}
