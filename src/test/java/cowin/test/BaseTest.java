package cowin.test;

import cowin.base.GlobalVariable;
import cowin.base.Page;

public class BaseTest {

	public static String stateName = GlobalVariable.STATE_NAME;
	public static String districtName = GlobalVariable.DISTRICT_NAME;
	public static String age = GlobalVariable.AGE;
	public static String dateOfVaccine = GlobalVariable.DATE_OF_VACCINE;
	public static String message = "";
	boolean statusForSlots = false;
	
	public void tearDown() {
		Page.driver.quit();
	}
}
