package cowin.locators;

public class HomePageLocators {

	public String searchByDistrict_CSS = ".status-switch";
	public String dropdownState_Xpath = "//mat-select[@formcontrolname='state_id']";
	public String stateAndDistrict_Xpath = "//span[@class='mat-option-text' and contains(.,'{index}')]";
	public String dropdownDistrict_Xpath = "//mat-select[@formcontrolname='district_id']";
	public String searchButton_Xpath = "//button[@class='pin-search-btn district-search']";
	public String dateList_Xpath = "//slide[@class='item carousel-item active ng-star-inserted']";
	public String date_Xpath = "//slide[@class='item carousel-item active ng-star-inserted']//p";
	public String slotsList_Xpath = "//div[@class='mat-main-field center-main-field']/div/div";
	public String slot_Xpath = "((//div[@class='slot-available-main col-padding col col-lg-9 col-md-9 col-sm-9 col-xs-12'])[{index01}]/ul/li)[{index02}]//a";
	public String age_Xpath = "//label[contains(.,'Age {index}')]";

}
