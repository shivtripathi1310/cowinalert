package cowin.page;

import java.util.LinkedList;
import java.util.List;

import javax.swing.event.ListDataEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cowin.base.Page;
import cowin.locators.HomePageLocators;
import cowin.utility.CommonUtilities;

public class HomePage extends Page{
	
	HomePageLocators homePageLocators;
	WebDriver driver;
	
	public HomePage(WebDriver driver){
		homePageLocators =  new HomePageLocators();
		this.driver = driver;
	}

	public void selectAreaByDistrict(String stateName,String districtName) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.querySelector('"+homePageLocators.searchByDistrict_CSS+"',':after').click();");

		click(homePageLocators.dropdownState_Xpath);
		click(getReplacedValue(homePageLocators.stateAndDistrict_Xpath,"[{]index[}]",stateName));

		Thread.sleep(1000);
		
		click(homePageLocators.dropdownDistrict_Xpath);
		click(getReplacedValue(homePageLocators.stateAndDistrict_Xpath,"[{]index[}]",districtName));
		click(homePageLocators.searchButton_Xpath);
	}
	
	public List<String> searchSlotsForAgeAndDate(String age,String districtName, String dateOfVaccine) {
		String date = CommonUtilities.getDate();
		List<String> messageList = new LinkedList<String>();
		click(getReplacedValue(homePageLocators.age_Xpath,"[{]index[}]",age));	

		List<WebElement> listOfCowinPlaces = getListOfWebElement(homePageLocators.slotsList_Xpath);
		String dateToBeCheckedIndex = String.valueOf(getIndexForTheDate(dateOfVaccine));
		String xpathForSlots = getReplacedValue(homePageLocators.slot_Xpath,"[{]index02[}]",dateToBeCheckedIndex);
		
		String messageText = "";
		String xpathValue="";
		for (int i = 1; i <= listOfCowinPlaces.size(); i++) {
			xpathValue = getReplacedValue(xpathForSlots,"[{]index01[}]",""+i+"");
			boolean status = findElementPresent(By.xpath(xpathValue));
			System.out.println("Status for the element visible : "+ status);
			if (status) {
				messageText = scrollToElement(By.xpath(xpathValue)).getText();
			} else {
				System.out.println("No vaccine center");
			}

			messageList.add("Available slot for " + date + " , location " + districtName + " : " + messageText);
		}
		
		return messageList;
	}
	
	public int getIndexForTheDate(String date) {
		List<WebElement> listDate = getListOfWebElement(homePageLocators.dateList_Xpath);
		int index=0;
		for(int i=1;i<=listDate.size();i++) {
			if(driver.findElement(By.xpath(homePageLocators.date_Xpath)).getText().contains(date)) {
				index = i;
				break;
			}
		}
		return index;
	}
}
