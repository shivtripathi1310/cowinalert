package cowin.base;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Page {

	public static String url = GlobalVariable.URL;
	public static String[] recepientMails = GlobalVariable.RECEPIENT_MAILS;
	public static String beepPath = "./src/test/resources/beep-06.mp3";


	public static WebDriver driver;

	public static void setup(String browserName) {

		if (browserName.equalsIgnoreCase("Chrome")) {
			WebDriverManager.chromedriver().setup();
			ChromeOptions options = new ChromeOptions();
			driver = new ChromeDriver(options);

		} else if (browserName.equalsIgnoreCase("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();

		} else if (browserName.equalsIgnoreCase("IE")) {
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		}

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(url);
	}

	public WebElement scrollToElement(By by) {
		WebElement element = driver.findElement(by);
		// Actions class with moveToElement()
		Actions a = new Actions(driver);
		a.moveToElement(element);
		a.perform();
		return element;
	}

	public boolean findElementPresent(By by) {
		try {
			WebElement element = driver.findElement(by);

		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	public void selectElement(WebElement element, String visibleText) {
		Select select = new Select(element);
		select.selectByVisibleText(visibleText);
	}

	public void click(String element) {

		scrollToElement(By.xpath(element)).click();
	}

	protected void sendKeys(String element, String value) {

		driver.findElement(By.xpath(element)).sendKeys(value);
	}

	protected List<WebElement> getListOfWebElement(String element) {

		return driver.findElements(By.xpath(element));
	}

	public String getReplacedValue(String element, String valueToBeReplaced, String replaceValue) {

		String value =  element.replaceAll(valueToBeReplaced, replaceValue);
		System.out.println(value);
		return value;
	}
}
