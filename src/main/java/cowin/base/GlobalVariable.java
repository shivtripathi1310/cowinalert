package cowin.base;

public class GlobalVariable {

		//Configuration for the Site
		public  static final String URL = "https://www.cowin.gov.in/home";
		public static final String STATE_NAME = "Write your stateName here";
		public static final String DISTRICT_NAME = "Write your district name here";
		public static final String AGE = "18";
		public static final String BROWSER = "Chrome";
		public static final String DATE_OF_VACCINE = "10 May";
		public static final int ITERATION_TIME_MIN = 1;  //It tells at what time duration program has to run.

		//Configuration for Email
		public static final String[] RECEPIENT_MAILS = { "recepient01@gmail.com" ,"recepient02@gmail.com"};
		public static final String SENDER_USERNAME = "myMail@gmail.com";
		public static final String SENDER_PASSWORD = "xxxxxxxxxxxx";

}
