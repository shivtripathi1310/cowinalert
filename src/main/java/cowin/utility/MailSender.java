package cowin.utility;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import cowin.base.GlobalVariable;

public class MailSender {
	
	private static String username = GlobalVariable.SENDER_USERNAME;
	private static String password = GlobalVariable.SENDER_PASSWORD;

	public static void mailSend(String[] recepientMails,String messageValue) {

		Properties props = new Properties();

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}

		});

		try {
			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress(username));

//			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recepientMail));
			for(String recepientMail : recepientMails) {
				message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(recepientMail));
			}
			message.setSubject("CoWin Slot Update");
			String htmlCode = "<h1>Hurry !!</h1><br/><h2>"+messageValue+"</h2>";
			message.setContent(htmlCode,"text/html");

			Transport.send(message);

			System.out.println("Email sent Successfully !");
		} catch (MessagingException e) {
			System.err.println("Something went wrong !");
			throw new RuntimeException(e);
		}
	}

}