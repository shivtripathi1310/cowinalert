package cowin.utility;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import cowin.base.Page;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class CommonUtilities {

	public static void sendMail(String[] recepientMails, String message) {
		System.out.println("Playing sound");
		CommonUtilities.playMusic();
		System.out.println("Sending mail");
		MailSender.mailSend(recepientMails, message);
	}
	
	public static void playMusic() {
		final JFXPanel fxPanel = new JFXPanel();
		Media hit = new Media(new File(Page.beepPath).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(hit);
		mediaPlayer.play();
	}
	
	public static String getDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String date = dtf.format(now);
		return date;
	}
}
